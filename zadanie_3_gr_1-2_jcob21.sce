//this code was created and tested  in SciLab 6.0.2 
//a 
//getting the matrix of the pm10 concentrations. Reading the information from  6 coulumn from 2 to 25 row , the data is seperated byt the ';' symbol.

    data1=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-08.csv',";",[],[],[],[],[2,6,25,6])
data1

//getting the matrix time connected to pm10 concentrations The function reads the information from 1 coulumn from 2 to 25 row , the date is seperated byt the ';' symbol.

    data1Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-08.csv',";",[],'string',[],[],[2,1,25,1])
data1Hour


// reapiting the same action as in data1 and data2 for rest of the days 
    data2=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-09.csv',";",[],[],[],[],[2,6,25,6])
data2

    data2Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-09.csv',";",[],'string',[],[],[2,1,25,1])
data2Hour

    data3=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-10.csv',";",[],[],[],[],[2,6,25,6])
data3

    data3Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-10.csv',";",[],'string',[],[],[2,1,25,1])
data3Hour

    data4=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-11.csv',";",[],[],[],[],[2,6,25,6])
data4

    data4Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-11.csv',";",[],'string',[],[],[2,1,25,1])
data4Hour

    data5=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-12.csv',";",[],[],[],[],[2,6,25,6])
data5

    data5Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-12.csv',";",[],'string',[],[],[2,1,25,1])
data5Hour

    data6=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-13.csv',";",[],[],[],[],[2,6,25,6])
data6

    data6Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-13.csv',";",[],'string',[],[],[2,1,25,1])
data6Hour

    data7=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-14.csv',";",[],[],[],[],[2,6,25,6])
data7

    data7Hour=csvRead('D:\program files\nauka\studia\matlab\data\dane-pomiarowe_2019-10-14.csv',";",[],'string',[],[],[2,1,25,1])
data7Hour

//function connects 2 matrixes into one, one contains strings and the other one numeric values 

function x=%c_a_s(a, b)
            x = [a,string(b)];
        endfunction

//connecting essential informations from week into one matrix 

    fullData=cat(1,data1,data2,data3,data4,data5,data6,data7)
    fullDataHour=cat(1,data1Hour,data2Hour,data3Hour,data4Hour,data5Hour,data6Hour,data7Hour)

// connectiong matrix of hours and matrix  pm10 concentrations into one 

    week=%c_a_s(fullDataHour,fullData)

// finding the maxiumum value   pm10 concentrations 

    maxFullData=max(fullData)

//showing asnwer 

    disp("maksymalna wartosc stezenia pm 10 to :")
    disp(maxFullData)

//opening ( if it does not exist creating) the file which will contains essencial data 

    pmResults = mopen('D:\program files\nauka\studia\matlab\pmResults.dat','a')

// putting infromaton into file 
    mputl("maksymalna wartosc stezenia pm 10 to :",pmResults)
    mfprintf(pmResults,'%d \n',maxFullData)
    mclose(pmResults)











































